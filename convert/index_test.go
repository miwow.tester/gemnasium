package convert

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
)

func TestIndex(t *testing.T) {
	included := []parser.Package{
		{Name: "a", Version: "1.0.0"},
		{Name: "a", Version: "2.0.0"},
		{Name: "b", Version: "2.0.0"},
	}
	excluded := []parser.Package{
		{Name: "a", Version: "3.0.0"},
		{Name: "c", Version: "1.0.0"},
	}
	index := NewIndex(included)

	tcs := []struct {
		id  uint
		pkg parser.Package
	}{
		{1, included[0]},
		{2, included[1]},
		{3, included[2]},
		{0, excluded[0]},
		{0, excluded[1]},
	}

	t.Run("IDOf", func(t *testing.T) {
		for _, tc := range tcs {
			want, got := tc.id, index.IDOf(tc.pkg)
			require.Equal(t, want, got)
		}
	})
}
