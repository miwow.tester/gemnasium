module gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/google/uuid v1.3.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spiegel-im-spiegel/go-cvss v0.4.0
	github.com/stretchr/testify v1.8.0
	github.com/umisama/go-cvss v0.0.0-20150430082624-a4ad666ead9b
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.8.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.17.0
	golang.org/x/tools v0.1.12
	gonum.org/v1/gonum v0.11.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/Microsoft/go-winio v0.5.1 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20211221144345-a4f6767435ab // indirect
	github.com/acomagu/bufpipe v1.0.3 // indirect
	github.com/bmatcuk/doublestar v1.3.4 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/go-git/gcfg v1.5.0 // indirect
	github.com/go-git/go-billy/v5 v5.3.1 // indirect
	github.com/go-git/go-git/v5 v5.4.2 // indirect
	github.com/imdario/mergo v0.3.12 // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/kevinburke/ssh_config v1.1.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/otiai10/copy v1.7.0 // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/spiegel-im-spiegel/errs v1.0.2 // indirect
	github.com/xanzy/ssh-agent v0.3.1 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0 // indirect
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce // indirect
	golang.org/x/exp v0.0.0-20191002040644-a1355ae1e2c3 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/text v0.4.0 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

go 1.19
