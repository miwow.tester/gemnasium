package finder

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPackageManager(t *testing.T) {
	t.Run("PURLTypeForPackageManager", func(t *testing.T) {
		var tcs = []struct {
			packageManagerName string
			purlType           string
		}{
			{"conan", "conan"},
			{"composer", "composer"},
			{"go", "golang"},
			{"npm", "npm"},
			{"nuget", "nuget"},
			{"yarn", "npm"},
			{"pnpm", "npm"},
			{"bundler", "gem"},
			{"maven", "maven"},
			{"gradle", "maven"},
			{"sbt", "maven"},
			{"pipenv", "pypi"},
			{"pip", "pypi"},
			{"poetry", "pypi"},
			{"unknown", "unknown-purl-type"},
		}

		for _, tc := range tcs {
			t.Run(tc.packageManagerName, func(t *testing.T) {
				got := PURLTypeForPackageManager(tc.packageManagerName)
				want := tc.purlType

				require.Equal(t, want, got)
			})
		}
	})

	t.Run("LanguageForPackageManager", func(t *testing.T) {
		var tcs = []struct {
			packageManagerName string
			language           string
		}{
			{"conan", "C/C++"},
			{"composer", "PHP"},
			{"go", "Go"},
			{"npm", "JavaScript"},
			{"nuget", "C#"},
			{"yarn", "JavaScript"},
			{"pnpm", "JavaScript"},
			{"bundler", "Ruby"},
			{"maven", "Java"},
			{"gradle", "Java"},
			{"sbt", "Scala"},
			{"pipenv", "Python"},
			{"pip", "Python"},
			{"poetry", "Python"},
			{"unknown", "unknown-language"},
		}

		for _, tc := range tcs {
			t.Run(tc.packageManagerName, func(t *testing.T) {
				got := LanguageForPackageManager(tc.packageManagerName)
				want := tc.language

				require.Equal(t, want, got)
			})
		}
	})
}
