package yarn

import (
	"bytes"
	"fmt"
	"io"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser/yarn/berry"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser/yarn/classic"
)

// Parse scans a yarn classic/berry lock file and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	// We need to keep two copies of the data because in case
	// we have a Yarn v1 lock file, parsing it as a yaml will fail
	// but `r` cannot be rewound back.
	b1 := bytes.NewBuffer(nil)
	_, err := io.Copy(b1, r)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to copy lock file data to buffer")
	}
	b2 := bytes.NewBuffer(b1.Bytes())

	isClassic, err := classic.IsValidLockfile(b1)
	if err != nil {
		return nil, nil, err
	}

	if isClassic {
		log.Debug("Classic Yarn lock file detected")
		return classic.Parse(b2)
	}

	// In case its Yarn berry
	return berry.Parse(b2)
}

func init() {
	parser.Register("yarn", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeNpm,
		Filenames:   []string{"yarn.lock"},
	})
}
