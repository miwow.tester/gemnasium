package npm

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser/testutil"
)

func TestNpm(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		t.Run("wrong version", func(t *testing.T) {
			fixture := testutil.Fixture(t, "wrong_versions", "package-lock.json")
			_, _, err := Parse(fixture, parser.Options{})
			require.EqualError(t, err, parser.ErrWrongFileFormatVersion.Error())
		})

		tcs := []struct {
			name            string
			fixtureName     string
			expectationName string
			options         parser.Options
		}{
			{
				"lockfile-v1/simple",
				"lockfile-v1/simple",
				"simple",
				parser.Options{IncludeDev: true},
			},
			{
				"lockfile-v1/simple/ignore-dev",
				"lockfile-v1/simple",
				"simple-ignore-dev",
				parser.Options{IncludeDev: false},
			},
			{
				"lockfile-v1/big",
				"lockfile-v1/big",
				"big",
				parser.Options{IncludeDev: true},
			},
			{
				"lockfile-v2/simple",
				"lockfile-v2/simple",
				"simple",
				parser.Options{IncludeDev: true},
			},
			{
				"lockfile-v2/simple/ignore-dev",
				"lockfile-v2/simple",
				"simple-ignore-dev",
				parser.Options{IncludeDev: false},
			},
			{
				"lockfile-v3/simple",
				"lockfile-v3/simple",
				"simple",
				parser.Options{IncludeDev: true},
			},
			{
				"lockfile-v3/simple/ignore-dev",
				"lockfile-v3/simple",
				"simple-ignore-dev",
				parser.Options{IncludeDev: false},
			},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc.fixtureName, "package-lock.json")
				got, _, err := Parse(fixture, tc.options)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc.expectationName, got)
			})
		}
	})
}
