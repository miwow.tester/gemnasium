package piplock

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser/testutil"
)

func TestPipLock(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tests := map[string]struct {
			fixtureDir     string
			expectationDir string
			opts           parser.Options
		}{
			"simple": {
				fixtureDir:     "simple",
				expectationDir: "simple",
				opts:           parser.Options{IncludeDev: true},
			},
			"big": {
				fixtureDir:     "big",
				expectationDir: "big",
				opts:           parser.Options{IncludeDev: true},
			},
			"old": {
				fixtureDir:     "old",
				expectationDir: "old",
				opts:           parser.Options{IncludeDev: true},
			},
			"simple_without_dev_dependencies": {
				fixtureDir:     "simple",
				expectationDir: "simple-no-dev",
				opts:           parser.Options{IncludeDev: false},
			},
			"big_without_dev_dependencies": {
				fixtureDir:     "big",
				expectationDir: "big-no-dev",
				opts:           parser.Options{IncludeDev: false},
			},
		}
		for name, tt := range tests {
			t.Run(name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tt.fixtureDir, "Pipfile.lock")
				got, _, err := Parse(fixture, tt.opts)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tt.expectationDir, got)
			})
		}
	})

	t.Run("Parse_Errors", func(t *testing.T) {
		tests := map[string]struct {
			fixtureDir string
			err        error
			opts       parser.Options
		}{
			"wrong_version": {
				fixtureDir: "wrong_version",
				err:        parser.ErrWrongFileFormat,
				opts:       parser.Options{},
			},
		}
		for name, tt := range tests {
			t.Run(name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tt.fixtureDir, "Pipfile.lock")
				_, _, err := Parse(fixture, tt.opts)
				require.Equal(t, err, tt.err)
			})
		}
	})
}
