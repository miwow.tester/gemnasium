package pnpm

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestExtractPackageNameAndVersion(t *testing.T) {
	tcs := []struct {
		lockfileVersion string
		dependencyPath  string
		wantName        string
		wantVersion     string
		wantErr         bool
	}{
		{
			"5.3",
			"/@next/swc-android-arm-eabi/13.0.0",
			"@next/swc-android-arm-eabi",
			"13.0.0",
			false,
		},
		{
			"5.4",
			"/request-promise-core/1.1.4_request@2.88.2",
			"request-promise-core",
			"1.1.4",
			false,
		},
		{
			"5.4",
			"/badly-formatted-string",
			"",
			"",
			true,
		},
		{
			"6.0",
			"/optionator@0.8.3",
			"optionator",
			"0.8.3",
			false,
		},
		{
			"6.0",
			"/request-promise-native@1.0.9(request@2.88.2)",
			"request-promise-native",
			"1.0.9",
			false,
		},
		{
			"6.0",
			"/acorn-globals@4.3.4",
			"acorn-globals",
			"4.3.4",
			false,
		},
		{
			"6.0",
			"/badly-formatted-version.3.4",
			"",
			"",
			true,
		},
	}

	for _, tc := range tcs {
		f := Lockfile{Version: tc.lockfileVersion}

		gotName, gotVersion, err := f.extractPackageNameAndVersion(tc.dependencyPath)

		if tc.wantErr {
			wantErrMsg := fmt.Errorf("unable to parse dependencyPath %q", tc.dependencyPath).Error()
			require.EqualError(t, err, wantErrMsg)
		} else {
			require.NoError(t, err)
		}

		require.Equal(t, tc.wantName, gotName)
		require.Equal(t, tc.wantVersion, gotVersion)
	}
}
