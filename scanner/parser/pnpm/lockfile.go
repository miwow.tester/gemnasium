package pnpm

import (
	"fmt"
	"regexp"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
)

// Lockfile is a pnpm lock file
type Lockfile struct {
	Version  string             `yaml:"lockfileVersion"`
	Packages map[string]Package `yaml:"packages"`
	parser.Options
}

// Package contains the details for a package entry in the packages section
type Package struct {
	// Name/Version are only present for packages that aren't in npm, for example, a tarball
	Name    string `yaml:"name"`
	Version string `yaml:"version"`
	Dev     bool   `yaml:"dev"`
}

// Returns whether the given lockfiel version can be parsed
func isSupportedVersion(version string) error {
	supportedVersions := []string{"5.3", "5.4", "6.0"}
	for _, supportedVersion := range supportedVersions {
		if version == supportedVersion {
			return nil
		}
	}

	return parser.ErrWrongFileFormatVersion
}

/*
version 5.x uses the following dependency path format:

/<package-name>/<package-version>_<parent-package>@<parent-version>

The parent-package@version after the underscore at the end is optional.

Examples:

	/@remix-run/router/1.1.0
	/@babel/helper-compilation-targets/7.20.7_@babel+core@7.20.7
	/utils-merge/1.0.1
*/
var v5regex = regexp.MustCompile(`^/(?P<Name>.*)/(?P<Version>[^_]*)`)

/*
version 6.x uses the following dependency path format:

/<package-name>@<package-version>(<parent-package>@<parent-version>)

The parent-package@version in parentheses at the end is optional.

Examples:

	/request-promise-native@1.0.9(request@2.88.2)
	/acorn-globals@4.3.4
*/
var v6regex = regexp.MustCompile(`^/(?P<Name>.*?)@(?P<Version>[^(]*)`)

func (f Lockfile) extractPackageNameAndVersion(dependencyPath string) (string, string, error) {
	re := v5regex

	if f.Version == "6.0" {
		re = v6regex
	}

	matches := re.FindStringSubmatch(dependencyPath)

	if len(matches) == 0 {
		return "", "", fmt.Errorf("unable to parse dependencyPath %q", dependencyPath)
	}

	return matches[re.SubexpIndex("Name")], matches[re.SubexpIndex("Version")], nil
}

// Parse returns packages without duplicates
func (f Lockfile) Parse() ([]parser.Package, error) {
	if err := isSupportedVersion(f.Version); err != nil {
		return nil, err
	}
	pkgs := make([]parser.Package, 0, len(f.Packages))

	for dependencyPath, pkg := range f.Packages {
		// skip package if the option to include devDependencies has been set to false.
		if pkg.Dev && !f.IncludeDev {
			continue
		}

		// pnpm lockfiles for tarballs contain Name and Version fields in the package details,
		// so we attempt to use those first, if available.
		name := pkg.Name
		version := pkg.Version

		// If the package Name or Version is unknown, attempt to obtain them by parsing the dependencyPath
		if name == "" || version == "" {
			var err error
			name, version, err = f.extractPackageNameAndVersion(dependencyPath)
			if err != nil {
				log.Warnf("pnpm parser: %s. Skipping dependency.", err)
				continue
			}
		}

		pkgs = append(pkgs, parser.Package{Name: name, Version: version})
	}

	return pkgs, nil
}
