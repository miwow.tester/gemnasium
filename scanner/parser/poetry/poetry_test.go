package poetry

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser/testutil"
)

func TestPoetry(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tests := map[string]struct {
			fixtureDir     string
			expectationDir string
			opts           parser.Options
		}{
			"simple": {
				fixtureDir:     "simple",
				expectationDir: "simple",
				opts:           parser.Options{IncludeDev: true},
			},
			"big": {
				fixtureDir:     "big",
				expectationDir: "big",
				opts:           parser.Options{IncludeDev: true},
			},
			"simple_without_dev_dependencies": {
				fixtureDir:     "simple",
				expectationDir: "simple-no-dev",
				opts:           parser.Options{IncludeDev: false},
			},
			"big_without_dev_dependencies": {
				fixtureDir:     "big",
				expectationDir: "big-no-dev",
				opts:           parser.Options{IncludeDev: false},
			},
		}

		for name, tt := range tests {
			t.Run(name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tt.fixtureDir, "poetry.lock")
				got, _, err := Parse(fixture, tt.opts)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tt.expectationDir, got)
			})
		}
	})

	t.Run("Parse_Errors", func(t *testing.T) {
		tests := map[string]struct {
			fixtureDir string
			opts       parser.Options
			err        error
		}{
			"missing_content_hash": {
				fixtureDir: "missing_content_hash",
				err:        parser.ErrWrongFileFormatVersion,
				opts:       parser.Options{IncludeDev: true},
			},
			"missing_python_versions": {
				fixtureDir: "missing_python_versions",
				err:        parser.ErrWrongFileFormatVersion,
				opts:       parser.Options{IncludeDev: true},
			},
		}
		for name, tt := range tests {
			t.Run(name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tt.fixtureDir, "poetry.lock")
				_, _, err := Parse(fixture, tt.opts)
				require.Equal(t, err, tt.err)
			})
		}
	})
}
