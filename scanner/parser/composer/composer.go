package composer

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
)

// Document contains a list of Packages
type Document struct {
	Packages    []Package `json:"packages"`
	PackagesDev []Package `json:"packages-dev"`
}

// Package contains info about the composer
type Package struct {
	Name    string `json:"name"`
	Version string `json:"version"` // Installed version
}

// Parse scans a Composer lock file and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, nil, fmt.Errorf("parsing composer.lock file: %v", err)
	}

	packages := make([]parser.Package, 0, len(document.Packages)+len(document.PackagesDev))
	for _, p := range document.Packages {
		packages = append(packages, parser.Package{Name: p.Name, Version: p.Version})
	}

	if !opts.IncludeDev {
		return packages, nil, nil
	}

	for _, p := range document.PackagesDev {
		packages = append(packages, parser.Package{Name: p.Name, Version: p.Version})
	}
	return packages, nil, nil
}

func init() {
	parser.Register("composer", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypePackagist,
		Filenames:   []string{"composer.lock"},
	})
}
