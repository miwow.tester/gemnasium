package nuget

import (
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
)

// pkgNameMap tracks packages by name.
// It can find and return packages that have been added.
// Package names are not case sensitive.
type pkgNameMap struct {
	packages map[string]*parser.Package
}

func (m *pkgNameMap) add(p *parser.Package) {
	key := strings.ToLower(p.Name)
	m.packages[key] = p
}

func (m pkgNameMap) seen(p parser.Package) bool {
	key := strings.ToLower(p.Name)
	_, set := m.packages[key]
	return set
}

func (m pkgNameMap) findByName(name string) (*parser.Package, bool) {
	key := strings.ToLower(name)
	pkg, found := m.packages[key]
	return pkg, found
}

func newPkgNameMap() *pkgNameMap {
	m := map[string]*parser.Package{}
	return &pkgNameMap{m}
}

// pkgVersionMap tracks packages by name and version.
// It can find and return packages that have been added.
// Package names are not case sensitive.
type pkgVersionMap struct {
	packages map[[2]string]*parser.Package
}

func (m *pkgVersionMap) add(p *parser.Package) {
	key := [2]string{strings.ToLower(p.Name), p.Version}
	m.packages[key] = p
}

func (m pkgVersionMap) seen(p parser.Package) bool {
	key := [2]string{strings.ToLower(p.Name), p.Version}
	_, set := m.packages[key]
	return set
}

func (m pkgVersionMap) find(p parser.Package) (*parser.Package, bool) {
	key := [2]string{strings.ToLower(p.Name), p.Version}
	pkg, found := m.packages[key]
	return pkg, found
}

func newPkgVersionMap() *pkgVersionMap {
	m := map[[2]string]*parser.Package{}
	return &pkgVersionMap{m}
}

// pkgDepMap tracks transitive dependencies.
// It can only tell whether a dependency has already been seen/added.
// Package names and dependency names are not case sensitive.
type pkgDepMap struct {
	dependencies map[[3]string]bool
}

func (m *pkgDepMap) add(p parser.Package, depName string) {
	key := [3]string{
		strings.ToLower(p.Name),
		p.Version,
		strings.ToLower(depName),
	}
	m.dependencies[key] = true
}

func (m pkgDepMap) seen(p parser.Package, depName string) bool {
	key := [3]string{
		strings.ToLower(p.Name),
		p.Version,
		strings.ToLower(depName),
	}
	_, set := m.dependencies[key]
	return set
}

func newPkgDepMap() *pkgDepMap {
	m := map[[3]string]bool{}
	return &pkgDepMap{m}
}
