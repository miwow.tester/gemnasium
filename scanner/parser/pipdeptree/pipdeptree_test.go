package pipdeptree

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser/testutil"
)

func TestPipdeptree(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		for _, tc := range []string{"pylons", "pony-forum"} {
			t.Run(tc, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc, "pipdeptree.json")
				got, _, err := Parse(fixture, parser.Options{})
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc, got)
			})
		}
	})
}
