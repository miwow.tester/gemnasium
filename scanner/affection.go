package scanner

import (
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/vrange"
)

// Affection combines an affected dependency with a security advisory.
type Affection struct {
	Dependency parser.Package    `json:"dependency"`
	Advisory   advisory.Advisory `json:"advisory"`
}

func (aff Affection) query() vrange.Query {
	return vrange.Query{
		Range:   aff.Advisory.AffectedRange,
		Version: aff.Dependency.Version,
	}
}
