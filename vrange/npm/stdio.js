#!/usr/bin/env node

const {processDocument} = require('./shared');

const writeResult = (res, lastDocument) => {
  process.stdout.write(JSON.stringify(res));
  process.stdout.write(lastDocument ? '\n' : '\r\n');
};

let restOfDocument = '';

process.stdin.resume();
process.stdin.setEncoding('utf8');

process.stdin.on('data', function(chunk) {
  let documents = (restOfDocument + chunk).split('\r\n');

  restOfDocument = documents.pop();

  documents.forEach(d => {
    writeResult(processDocument(d));
  });
});

process.stdin.on('end', function() {
  writeResult(processDocument(restOfDocument), true);
});
