package python

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/vrange"
)

/*
The configEnvVars configure the options for all pipenv commands via env variables.

PIPENV_VENV_IN_PROJECT=1: This forces pipenv to use the virtualenv that was created within
the /vrange/python project directory.

PIPENV_KEEP_OUTDATED=1: This prevents the project from fetching new dependencies and uses
the ones packaged in projects local virtualenv. Specifically, this guards against
connectivity errors when running in offline environments.
*/
var configEnvVars = []string{"PIPENV_VENV_IN_PROJECT=1", "PIPENV_KEEP_OUTDATED=1"}

func init() {
	vrange.Register("python", &resolver{})
}

type resolver struct {
	dir string
}

func (r *resolver) Resolve(queries []vrange.Query) (*vrange.ResultSet, error) {
	tmpfile, err := os.CreateTemp("/tmp", "vrange_queries")
	if err != nil {
		return nil, err
	}
	defer os.Remove(tmpfile.Name())
	if err := json.NewEncoder(tmpfile).Encode(queries); err != nil {
		return nil, err
	}
	if err := tmpfile.Close(); err != nil {
		return nil, err
	}

	rangecheck := filepath.Join(r.dir, "rangecheck.py")

	var stdErr bytes.Buffer
	cmd := exec.Command("pipenv", "run", rangecheck, tmpfile.Name())
	cmd.Stderr = &stdErr
	cmd.Dir = r.dir
	cmd.Env = append(cmd.Env, configEnvVars...)
	out, err := cmd.Output()
	log.Debugf("%s\n%s", cmd.String(), out)
	if stdErr.Len() != 0 {
		log.Warnf("%s\n%s", cmd.String(), stdErr.String())
	}
	if err != nil {
		return nil, fmt.Errorf("running python vrange command dir: %q cmd: %q err: %v", cmd.Dir, cmd.String(), err)
	}
	results := []struct {
		Version   string
		Range     string
		Satisfies bool
		Error     string
	}{}
	if err := json.Unmarshal(out, &results); err != nil {
		return nil, err
	}

	set := make(vrange.ResultSet)
	for _, result := range results {
		var err error
		if result.Error != "" {
			err = errors.New(result.Error)
		}
		query := vrange.Query{Version: result.Version, Range: result.Range}
		set.Set(query, result.Satisfies, err)
	}
	return &set, nil
}

func (*resolver) Flags() []cli.Flag {
	return nil
}

func (r *resolver) Configure(c *cli.Context, opts vrange.Options) error {
	r.dir = filepath.Join(opts.BaseDir, "python")
	return nil
}
