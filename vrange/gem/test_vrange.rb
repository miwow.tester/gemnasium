# frozen_string_literal: true

# can be executed with `rake test`

require 'test/unit'
require 'open3'
require 'logger'

class TestRangecheck < Test::Unit::TestCase
  def setup
    @logger = Logger.new(STDOUT)
    @executable = './vrange.rb'
  end

  def cleanupstr(str)
    str.gsub(/[^0-9A-Za-z{}"]/, '')
  end

  def test_wellformed_json_file
    expect = File.read('tests/simple_out.json')
    stdout, stderr, status = Open3.capture3("#{@executable} tests/simple_in.json")
    result = stdout.empty? ? stderr : stdout
    assert_equal(cleanupstr(result), cleanupstr(expect))
    assert(status.success?)
  end

  def test_dictionary_json_file
    stdout, stderr, status = Open3.capture3("#{@executable} tests/dictionary.json")
    expect = <<~EXPECT
      [0-9]+: unexpected token at '{
      {"range": "==2.0.1", "version": "2.0.0"}
      }
      '
    EXPECT
    result = stdout.empty? ? stderr : stdout
    assert_match(/#{expect}/, result)
    assert(!status.success?)
  end

  def test_empty_json_file
    stdout, stderr, status = Open3.capture3("#{@executable} tests/empty.json")
    result = stdout.empty? ? stderr : stdout
    assert_equal(result, "JSON Array expected\n")
    assert(!status.success?)
  end

  def test_nonexistent_json_file
    stdout, stderr, status = Open3.capture3("#{@executable} tests/empty00.json")
    result = stdout.empty? ? stderr : stdout
    assert_equal(result, "No such file or directory @ rb_sysopen - tests/empty00.json\n")
    assert(!status.success?)
  end

  def test_gemnasium_db_advisory_ranges
    expect = File.read('tests/adb_ranges_out.json')
    stdout, stderr, status = Open3.capture3("#{@executable} tests/adb_ranges_in.json")
    result = stdout.empty? ? stderr : stdout
    assert_equal(cleanupstr(result), cleanupstr(expect))
    assert(status.success?)
  end
end
